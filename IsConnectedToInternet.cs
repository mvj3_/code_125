//判断网路状态使用如下代码
private bool IsConnectedToInternet()
{
    bool connected = false;

    ConnectionProfile cp = NetworkInformation.GetInternetConnectionProfile();

    if (cp != null)
    {
        NetworkConnectivityLevel cl = cp.GetNetworkConnectivityLevel();

        connected = cl == NetworkConnectivityLevel.InternetAccess;
    }

    return connected;
}