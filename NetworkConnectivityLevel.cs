//NetworkInformation 类属于static类，里面的方法也是static的，所以，这里通过直接访问

//GetInternetConnectionProfile 方法，获得网络连接信息，然后根据NetworkConnectivityLevel  来区分网络的链接状态。
//NetworkConnectivityLevel  的定义如下。
    // 摘要:
    //     定义当前可用连接的级别。
    [Version(100794368)]
    public enum NetworkConnectivityLevel
    {
        // 摘要:
        //     无连接。
        None = 0,
        //
        // 摘要:
        //     仅本地网络访问。
        LocalAccess = 1,
        //
        // 摘要:
        //     受限的 internet 访问。
        ConstrainedInternetAccess = 2,
        //
        // 摘要:
        //     本地和 internet 访问。
        InternetAccess = 3,
    }