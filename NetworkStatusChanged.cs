//监听网络状态的改变，则使用如下代码，并结合上面的代码。通过监听NetworkStatusChanged  事件即可，当网络状态发生改变时，该事件会被触发
NetworkInformation.NetworkStatusChanged += (object sener) =>
{
    if (!IsConnectedToInternet())
    {
        // 网络不可以访问
    }
    else
    {
        // 网络可以访问
    }
};